const express = require('express');

const router = express.Router();
const categoriesController = require('../controllers/categories');

router.post('/category', categoriesController.createCategory)

router.get('/category/:id', categoriesController.getCategory);

router.put('/category/:id', categoriesController.updateCategory);

router.get('/categories', categoriesController.getAllCategory);

router.delete('/category/:id', categoriesController.deleteCategory);

module.exports = router;
