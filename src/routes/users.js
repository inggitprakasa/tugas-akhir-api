const express = require('express');

const router = express.Router();
const usersController = require('../controllers/users');

router.post('/user', usersController.createUser)

router.get('/user/:id', usersController.getUser);

router.put('/user/:id', usersController.updateUser);

router.get('/users', usersController.getAllUser);

router.delete('/user/:id', usersController.deleteUser);

router.post('/login',usersController.loginUser);

router.post('/register', usersController.registerUser);

module.exports = router;
