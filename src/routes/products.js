const express = require('express');

const router = express.Router();
const productsController = require('../controllers/products');

router.post('/product', productsController.createProduct)

router.get('/product/:id', productsController.getProduct);
//
router.put('/product/:id', productsController.updateProduct);
//
router.get('/products', productsController.getAllProduct);
//
router.delete('/product/:id', productsController.deleteProduct);

module.exports = router;
