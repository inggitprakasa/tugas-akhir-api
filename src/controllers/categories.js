const Category = require('../models/Category');
const path = require('path');
const fs = require('fs');

exports.createCategory = (req, res, next) => {

    const { title,description,status } = req.body

    const category = new Category({title,description,status})

    category.save()
        .then(result => {
            res.status(201).json({
                message : "Category Created",
                data : result
            });
            next();
        })
        .catch(err => {
            console.log("Err : ", err);
        });
}

exports.getAllCategory = (req, res, next) => {
    Category.find()
        .then(result =>{
            res.status(200).json({
                message : "Success",
                data : result
            })
            next();
        }).catch(err => {
        console.log(err)
    })
}

exports.getCategory = (req,res,next) => {
    Category.findById(req.params.id)
        .then(result => {
            res.status(200).json({
                message : "Success",
                data : result
            })
            next();
        })
        .catch(err => {
            console.log(err)
        })
}

exports.updateCategory = (req,res,next) => {
    const title  = req.body.title;
    const description = req.body.description;
    const status = req.body.status;
    const id = req.params.id;

    Category.findById(id).then(result => {
        result.title = title;
        result.description = description;
        result.status = status;

        return result.save()
    }).then(value => {
        res.status(200).json({
            message : "Update Success",
            data : value
        })
        next();
    }).catch(err => {
        console.log(err)
    })
}

exports.deleteCategory = (req, res, next) => {
    const {id} = req.params;

    Category.findById(id).then(post => {
        return Category.findByIdAndRemove(id);
    }).then(result => {
        res.status(200).json({
            message : "Delete Success",
            data : {}
        })
    }).catch( err => {
        console.log(err)
    })
}
