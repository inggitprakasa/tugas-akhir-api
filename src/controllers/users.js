// Mengumpulkan semua fungsi
const User = require('../models/User');
const path = require('path');
const fs = require('fs');
const jsonwebtoken =  require('jsonwebtoken');
const bcryptjs = require('bcryptjs');

exports.createUser = (req, res, next) => {
    const username = req.body.username;
    const password = req.body.password;
    const image = req.file.path;

    const user = new User({ username, password, image})

    user.save()
        .then(result => {
            res.status(201).json({
                message : "User Created",
                data : result
            });
            next();
        })
        .catch(err => {
            console.log("Err : ", err);
        });
}

exports.getAllUser = (req, res, next) => {
    User.find()
        .then(result =>{
            res.status(200).json({
                message : "Success",
                data : result
            })
            next();
        }).catch(err => {
            console.log(err)
    })
}

exports.getUser = (req,res,next) => {
    User.findById(req.params.id)
        .then(result => {
            res.status(200).json({
                message : "Success",
                data : result
            })
            next();
        })
        .catch(err => {
            console.log(err)
        })
}

exports.loginUser = async (req, res) => {
    const { username , password } = req.body;
    const dataUser = await User.findOne({username: username})
    const emailUser = await User.findOne({email: username})
    if (dataUser || emailUser) {
        const passUser = await bcryptjs.compare(password, dataUser.password)
        if (passUser) {
            const data = {
                id : dataUser._id
            }
            const token = await jsonwebtoken.sign(data,"12345678")
            return res.status(200).json({
                message : 'Login Success',
                token : token
            })
        }
    }
    return res.status(403).json({
        message : "Incorect username or password"
    })
}

exports.registerUser = async (req,res) => {
    const { username, email, password } = req.body;
    const image = req.file.path;

    const hasOneUsername = User.findOne({ username : username});

    // Cek Username yang sama, Username tidak Boleh sama
    if (hasOneUsername ) {
        return res.status(403).json({
            message : "Username already used, please try again"
        })
    }

    // Hash password user
    const hashPass = await bcryptjs.hash(password, 10);
    const user = new User({ username ,email , password : hashPass, image})

    user.save()
        .then(result => {
            res.status(201).json({
                message : "User Created",
                data : {
                    _id : result._id
                }
            });
        })
        .catch(err => {
            res.status(500).json({
                message : err.message
            })
        });

}

exports.updateUser = (req,res,next) => {
    const username = req.body.username;
    const password = req.body.password;
    const image = req.file.path;
    const id = req.params.id;

    User.findById(id).then(result => {
        result.username = username;
        result.password = password;
        result.image = image;

        return result.save()
    }).then(value => {
        res.status(200).json({
            message : "Update Success",
            data : value
        })
        next();
    }).catch(err => {
        console.log(err)
    })
}

exports.deleteUser = (req, res, next) => {
    const id = req.params.id;

    User.findById(id).then(post => {
        removeImages(post.image);
        return User.findByIdAndRemove(id);
    }).then(result => {
        res.status(200).json({
            message : "Delete Success",
            data : {}
        })
    }).catch( err => {
        console.log(err)
    })
}

const removeImages = (filePath) => {
    const pathFile = path.join(__dirname, '../..' ,filePath);
    fs.unlink(pathFile, err => {
        console.log(err)
    });
}
