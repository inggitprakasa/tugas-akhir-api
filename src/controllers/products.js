// Mengumpulkan semua fungsi

const Product = require('../models/Product');
const path = require('path');
const fs = require('fs');

exports.createProduct = (req, res, next) => {
    const title  = req.body.title;
    const description = req.body.description;
    const status = req.body.status;
    const price = req.body.price;
    const tags = req.body.tags;
    const category = req.body.category;
    const image = req.file.path;

    const product = new Product({title,description,status,tags,price,category,image})

    product.save()
        .then(result => {
            res.status(201).json({
                message : "Product Created",
                data : result
            });
            next();
        })
        .catch(err => {
            console.log("Err : ", err);
        });
}

exports.getAllProduct = (req, res, next) => {
    Product.find()
        .then(result =>{
            res.status(200).json({
                message : "Success",
                data : result
            })
            next();
        }).catch(err => {
        console.log(err)
    })
}

exports.getProduct = (req,res,next) => {
    Product.findById(req.params.id)
        .then(result => {
            res.status(200).json({
                message : "Success",
                data : result
            })
            next();
        })
        .catch(err => {
            console.log(err)
        })
}

exports.updateProduct = (req,res,next) => {
    const title  = req.body.title;
    const description = req.body.description;
    const status = req.body.status;
    const price = req.body.price;
    const tags = req.body.tags;
    const category = req.body.category;
    const image = req.file.path;
    const id = req.params.id;

    Product.findById(id).then(result => {
        result.title = title;
        result.description = description;
        result.status = status;
        result.price = price;
        result.tags = tags;
        result.category = category;
        result.image = image;

        return result.save()
    }).then(value => {
        res.status(200).json({
            message : "Update Success",
            data : value
        })
        next();
    }).catch(err => {
        console.log(err)
    })
}

exports.deleteProduct = (req, res, next) => {
    const id = req.params.id;

    Product.findById(id).then(post => {
        removeImages(post.image);
        return Product.findByIdAndRemove(id);
    }).then(result => {
        res.status(200).json({
            message : "Delete Success",
            data : {}
        })
    }).catch( err => {
        console.log(err)
    })
}

const removeImages = (filePath) => {
    const pathFile = path.join(__dirname, '../..' ,filePath);
    fs.unlink(pathFile, err => {
        console.log(err)
    });
}
