const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const usersRoutes = require('./src/routes/users');
const productsRoutes = require('./src/routes/products');
const categoriesRoutes = require('./src/routes/categories');
const mongoose = require('mongoose');
const multer = require('multer');
const path = require('path');
const InitiateMongoServer = require('./src/config/db');
const { port } = require('./src/config/config');

// app.use((req, res, next) => {
//     res.setHeader('Access-Control-Allow-Origin','*');
//     res.setHeader('Access-Control-Allow-Headers','Content-Type, Authorization');
//     res.setHeader('Access-Control-Allow-Methods','GET, POST, PUT, DELETE');
// })

// Connect DB
InitiateMongoServer();

const fileStorage = multer.diskStorage({
    destination : (req, file, callback) => {
        callback(null, 'images')
    },
    filename(req, file, callback) {
        callback(null, new Date().getTime() + "-" + file.originalname)
    }
})

const filefilter = (req, file, callback) => {
    if( file.mimetype === 'image/png' ||
        file.mimetype === 'image/jpg' ||
        file.mimetype === 'image/jpeg')
    {
        callback(null, true)
    } else {
        callback(null, false)
    }
}

// PORT
const PORT = port || 4000;

app.use(bodyParser.json());
app.use(multer({storage : fileStorage, fileFilter : filefilter}).single('image'));
app.use('/images', express.static(path.join(__dirname, 'images')))
app.use('/', usersRoutes);
app.use('/', productsRoutes);
app.use('/', categoriesRoutes);

app.listen(PORT, (req, res) => {
    console.log(`Server Started at PORT ${PORT}`);
});
